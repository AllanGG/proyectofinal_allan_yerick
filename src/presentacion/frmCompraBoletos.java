/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;


import Clases.Compras;
import Clases.Logica_Compra;
import Clases.Logica_Ruta;
import Clases.Logica_Terminal;
import Clases.Logica_Unidad;
import Clases.Ruta;
import Clases.Terminal;
import Clases.Unidad;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JOptionPane;
import negocio.Administrador;
import negocio.Logica_data_usr;

public class frmCompraBoletos extends javax.swing.JFrame {

    public static String ceduU;
    public static String[][] matriz = {
        {"  ", "  ", "  ", "  ", "CC", "  ", "  ", "  "},
        {"  ", "1A", "1B", "1C", "CC", "1D", "1E", "1F"},
        {"  ", "2A", "2B", "2C", "CC", "2D", "2E", "2F"},
        {"  ", "3A", "3B", "3C", "CC", "3D", "3E", "3F"},
        {"  ", "4A", "4B", "4C", "CC", "4D", "4E", "4F"},
        {"  ", "5A", "5B", "5C", "CC", "5D", "5E", "5F"},
        {"  ", "6A", "6B", "6C", "CC", "6D", "6E", "6F"}};
    
    ArrayList<String> campos = new ArrayList<String>();
    int precioTotal;
    public frmCompraBoletos(String cedu) {
        
        initComponents();
        setLocationRelativeTo(null);
        btnComprar.setEnabled(false);
        llenar();
        ceduU=cedu;
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnRegresar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        cmbRutas = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        spnAsientos = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblAsientos = new javax.swing.JTable();
        btnComprar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtCosto = new javax.swing.JTextField();
        btnConfirmar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        lblAsientos = new javax.swing.JLabel();
        btnBusqueda = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnRegresar.setText("Regresar");
        btnRegresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegresarActionPerformed(evt);
            }
        });

        jLabel1.setText("Ruta:");

        jLabel2.setText("Pasajeros:");

        spnAsientos.setModel(new javax.swing.SpinnerNumberModel(1, 0, 5, 1));

        jLabel3.setText("Comprar Boletos:");

        tblAsientos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"1", "1A", "1B", "1C", null, "1D", "1E", "1F"},
                {"2", "2A", "2B", "2C", null, "2D", "2E", "2F"},
                {"3", "3A", "3B", "3C", null, "3D", "3E", "3F"},
                {"4", "4A", "4B", "4C", null, "4D", "4E", "4F"},
                {"5", "5A", "5B", "5C", null, "5D", "5E", "5F"},
                {"6", "6A", "6B", "6C", null, "6D", "6E", "6F"}
            },
            new String [] {
                "Filas", "A", "B", "C", "", "D", "E", "F"
            }
        ));
        tblAsientos.setEnabled(false);
        jScrollPane1.setViewportView(tblAsientos);

        btnComprar.setText("Comprar");
        btnComprar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnComprarActionPerformed(evt);
            }
        });

        jLabel5.setText("Asientos:");

        txtCosto.setEditable(false);
        txtCosto.setText("Costo C/U");
        txtCosto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCostoActionPerformed(evt);
            }
        });

        btnConfirmar.setText("Confirmar");
        btnConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmarActionPerformed(evt);
            }
        });

        jLabel4.setText("Total:");

        lblTotal.setText("total");

        lblAsientos.setText("        ");

        btnBusqueda.setText("Busqueda");
        btnBusqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBusquedaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnRegresar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnBusqueda))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(49, 49, 49)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(cmbRutas, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(29, 29, 29)
                                        .addComponent(txtCosto, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addGap(18, 18, 18)
                                        .addComponent(lblAsientos, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGap(39, 39, 39)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel4)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addGap(6, 6, 6)
                                                    .addComponent(btnComprar)))
                                            .addComponent(lblTotal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(26, 26, 26)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel2)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(spnAsientos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(btnConfirmar)))))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(176, 176, 176)
                                .addComponent(jLabel3)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRegresar)
                    .addComponent(btnBusqueda))
                .addGap(11, 11, 11)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmbRutas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(spnAsientos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCosto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnConfirmar)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(lblTotal))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(lblAsientos)
                    .addComponent(btnComprar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRegresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegresarActionPerformed
        new frmIniciarSesion().setVisible(true);
        super.dispose();
    }//GEN-LAST:event_btnRegresarActionPerformed
    
    
    private void btnConfirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmarActionPerformed
        int cantidad = (int) spnAsientos.getValue();
        String ruta = cmbRutas.getSelectedItem().toString();
        campos.clear();
        int precio = 0;
        String tempo= new String();
        if(cantidad == 0){
            JOptionPane.showMessageDialog(null, "Seleccione entre 1 y 5 asientos");
        }else if(cantidad > 0){
            
            Logica_Ruta oRut = new Logica_Ruta();
            ArrayList<Ruta> list = oRut.obtener();
            for (int i = 0; i < list.size(); i++) {
                tempo = list.get(i).getIdRuta();
                if(tempo.equals(ruta)){
                    precio = list.get(i).getPrecio();
                }
                
            }
            precioTotal=precio*cantidad;
            lblTotal.setText(precioTotal+"");
                    
            txtCosto.setText(precio+"");
            int seleccionados = 0;
            for (int x=1; x <=6; x++) {
                for (int y=1; y <= 7; y++) {
                    if((matriz[x][y] != "XX") && (matriz[x][y] != "CC")){
                        seleccionados = seleccionados + 1;
                        campos.add(matriz[x][y]);
                        matriz[x][y] = "XX";
                        
                        
                    }
                    if(cantidad==seleccionados){
                        if(y != 7){
                            matriz[x][y+1] = "XX";
                        }
                        break;
                    }
                }
                if(cantidad==seleccionados){
                        break;
                }
            }
            lblAsientos.setText(campos.toString());
            btnComprar.setEnabled(true);
        }
    }//GEN-LAST:event_btnConfirmarActionPerformed

    private void btnBusquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBusquedaActionPerformed
        new frmBuscador(ceduU).setVisible(true);
        super.dispose();
    }//GEN-LAST:event_btnBusquedaActionPerformed

    private void txtCostoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCostoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCostoActionPerformed

    private void btnComprarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnComprarActionPerformed
        guardarDatos();
    }//GEN-LAST:event_btnComprarActionPerformed

    
    public void guardarDatos() {
        int cantidad = (int) spnAsientos.getValue();    
        Logica_Compra agregar = new Logica_Compra();
        Compras compra = new Compras();
        int edad=0;
        compra.setCedula(ceduU);
        
        String genero= new String();
        Logica_data_usr oUsr = new Logica_data_usr();
        ArrayList<Administrador> list = oUsr.obtener();
        for (int i = 0; i < list.size(); i++) {
            
            int cedu2 = list.get(i).getCedula();
            String cedu3 = Integer.toString(cedu2);
            if(cedu3.equals(ceduU)){
                edad= list.get(i).getEdad();
                genero = list.get(i).getGenero();
            }

        }
        
        compra.setGenero(genero);
        
        
        String origen = new String();
        String destino = new String();
        Logica_Ruta oRut = new Logica_Ruta();
        ArrayList<Ruta> list2 = oRut.obtener();
        String idRuta = new String();
        idRuta = cmbRutas.getSelectedItem().toString();
        for (int i = 0; i < list2.size(); i++) {
            
            if(idRuta.equals(list2.get(i).getIdRuta())){
                origen = list2.get(i).getOrigen().toString();
                destino = list2.get(i).getDestino().toString();
                
            }

        }
        
        
        compra.setSalida(origen);
        compra.setLlegada(destino);
        compra.setIntermedio("");
        
            
            
        Calendar cal=Calendar.getInstance();
        String fecha=cal.get(cal.DATE)+"/"+cal.get(cal.MONTH)+"/"+cal.get(cal.YEAR);
        String hora=cal.get(cal.HOUR_OF_DAY)+":"+cal.get(cal.MINUTE);
            
        compra.setFechaHora(fecha+" "+hora);
        compra.setCantidad(cantidad);
        compra.setAsientos(lblAsientos.getText());
        compra.setDuracion("1");
        if(edad>=65){
            precioTotal=precioTotal/2;
        }
        compra.setCosto(precioTotal);
        
            
        agregar.agregar(compra);
            
        JOptionPane.showMessageDialog(this, "Datos guardados");  
    }
    
    
    public void llenar() {
        ArrayList<String> campos = new ArrayList<String>();
        Logica_Ruta oRut = new Logica_Ruta();
        ArrayList<Ruta> list = oRut.obtener();
        for (int i = 0; i < list.size(); i++) {
            
            campos.add(((Ruta) list.get(i)).getIdRuta());

        }
       cmbRutas.addItem("Seleccionar Ruta");
       for (int i = 0; i < list.size(); i++){
           cmbRutas.addItem(campos.get(i).toString());
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmCompraBoletos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmCompraBoletos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmCompraBoletos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmCompraBoletos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBusqueda;
    private javax.swing.JButton btnComprar;
    private javax.swing.JButton btnConfirmar;
    private javax.swing.JButton btnRegresar;
    private javax.swing.JComboBox<String> cmbRutas;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblAsientos;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JSpinner spnAsientos;
    private javax.swing.JTable tblAsientos;
    private javax.swing.JTextField txtCosto;
    // End of variables declaration//GEN-END:variables
}
