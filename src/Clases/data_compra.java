/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author allan
 */
public class data_compra {

    private static String path;
    private static BufferedWriter escribir;
    private static BufferedReader leer;

    public data_compra() {
        try {
            path = new File("src/archivos/Compras.txt").getCanonicalPath();
        } catch (IOException ex) {
            Logger.getLogger(data_termi.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    public void crear_archivo(String nombre_archivo, Boolean agregar) throws IOException {
        File archivo = new File(nombre_archivo);

        if (archivo.exists() && !archivo.isFile()) { // Pregunta si el archivo existe, de ser así valida que realmente sea un archivo
            throw new IOException(archivo.getName() + " no es un archivo"); // No no cumple, el programa imprime este mensaje
        }

        escribir = new BufferedWriter(new FileWriter(archivo, agregar));
    }

    public void cerrar_archivo() throws IOException {
        escribir.close();
    }

    public Boolean existe(Compras object) {
        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;

            while ((line = leer.readLine()) != null) {
                String[] data = line.split("-");

                if (data[0].equals(String.valueOf(object.getCedula()))) {
                    return true;
                }
            }
            leer.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(data_compra.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(data_compra.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public Boolean agregar(Compras object) {
        try {
            this.crear_archivo(this.path, true);

            if (!this.existe(object)) {
                object.setCodigo(this.autoincremental());
                escribir.write(String.valueOf(object.getCodigo())+"-");
                escribir.write(String.valueOf(object.getCedula()) + "-");
                escribir.write(object.getSalida() + "-");
                escribir.write(object.getLlegada() + "-");
                escribir.write(object.getIntermedio() + "-");
                escribir.write(object.getFechaHora() + "-");
                escribir.write(String.valueOf(object.getCantidad()) + "-");
                escribir.write(object.getAsientos() + "-");
                escribir.write(object.getDuracion() + "-");
                escribir.write(String.valueOf(object.getCosto()) + "-");
                escribir.write(System.lineSeparator());

                this.cerrar_archivo();
                return true;
            }
        } catch (IOException ex) {
            Logger.getLogger(data_compra.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return false;
    }

    public Boolean editar(Compras object) {
        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;
            StringBuilder cadena = new StringBuilder();

            while ((line = leer.readLine()) != null) {
                String[] data = line.split("-");

                if (data[0].equals(String.valueOf(object.getCodigo()))) {
                    cadena.append(String.valueOf(
                            object.getCodigo() + "-"
                            + object.getCedula() + "-"
                            + object.getSalida() + "-"
                            + object.getLlegada() + "-"
                            + object.getIntermedio() + "-"
                            + object.getFechaHora() + "-"
                            + object.getCantidad() + "-"
                            + object.getAsientos() + "-"
                            + object.getDuracion() + "-"
                            + object.getCosto()
                            + System.lineSeparator())
                    );
                } else {
                    cadena.append(line + System.lineSeparator());
                }
            }
            leer.close();
            this.crear_archivo(this.path, false);
            this.crear_archivo(this.path, true);
            escribir.write(cadena.toString());
            this.cerrar_archivo();
            return true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(data_compra.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(data_compra.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public Boolean eliminar(Compras object) {
        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;
            StringBuilder cadena = new StringBuilder();

            while ((line = leer.readLine()) != null) {
                String[] data = line.split("-");

                if (!data[0].equals(String.valueOf(object.getCedula()))) {
                    cadena.append(line + System.lineSeparator());
                }
            }
            leer.close();
            this.crear_archivo(this.path, false);
            this.crear_archivo(this.path, true);
            escribir.write(cadena.toString());
            this.cerrar_archivo();
            return true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(data_compra.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(data_compra.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public ArrayList obtener() {
        ArrayList<Compras> admin = new ArrayList();

        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;
            StringBuilder cadena = new StringBuilder();

            while ((line = leer.readLine()) != null) {
                String[] data = line.split("-");
                Compras pasa = new Compras();
                pasa.setCodigo(Integer.parseInt(data[0]));
                pasa.setCedula(Integer.parseInt(data[1]));
                pasa.setSalida(data[2]);
                pasa.setLlegada(data[3]);
                pasa.setIntermedio(data[4]);
                pasa.setFechaHora(data[5]);
                pasa.setCantidad(Integer.parseInt(data[6]));
                pasa.setAsientos((data[7]));
                pasa.setDuracion((data[8]));
                pasa.setCosto(Integer.parseInt(data[9]));
                admin.add(pasa);
            }
            leer.close();
            return admin;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(data_compra.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(data_compra.class.getName()).log(Level.SEVERE, null, ex);
        }
        return admin;
    }

    public int autoincremental() {
        int autoincremental = 0;

        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;
            StringBuilder cadena = new StringBuilder();

            while ((line = leer.readLine()) != null) {
                String[] data = line.split("-");

                if (autoincremental < Integer.parseInt(data[0])) {
                    autoincremental = Integer.parseInt(data[0]);
                }
            }
            leer.close();
            return autoincremental + 1;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(data_compra.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(data_compra.class.getName()).log(Level.SEVERE, null, ex);
        }
        return autoincremental;
    }

}

