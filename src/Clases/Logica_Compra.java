/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.util.ArrayList;



public class Logica_Compra {
    private boolean hay_error;
    private String mensaje_error;
    data_compra oCom = new data_compra();
  
    public Boolean agregar(Compras compra) {
        this.setHay_error(oCom.agregar(compra));
        
        if (!this.isHay_error()) {
            this.setMensaje_error("No se agrego el administrador");
        }
        return this.isHay_error(); 
    }
    
    public Boolean editar(Compras compra) {
        this.setHay_error(oCom.editar(compra));
        
        if (!this.isHay_error()) {
            this.setMensaje_error("No se edito el administrador");
        }
        return this.isHay_error(); 
    }

    public Boolean eliminar(Compras compra) {
        this.setHay_error(oCom.eliminar(compra));
        
        if (!this.isHay_error()) {
            this.setMensaje_error("No se elimino el administrador");
        }
        return this.isHay_error(); 
    }
    
    public ArrayList<Compras> obtener() {
        ArrayList<Compras> compra= oCom.obtener();
        return compra;
    }

    public boolean isHay_error() {
        return hay_error;
    }

    public void setHay_error(boolean hay_error) {
        this.hay_error = hay_error;
    }

    public String getMensaje_error() {
        return mensaje_error;
    }

    public void setMensaje_error(String mensaje_error) {
        this.mensaje_error = mensaje_error;
    }
    
}

