/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;
 
import java.awt.List;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author admin
 */
public class data_ruta {

    private static String path;
    private static BufferedWriter escribir;
    private static BufferedReader leer;

    public data_ruta() {
        try {
            path = new File("src/archivos/Rutas.txt").getCanonicalPath();
        } catch (IOException ex) {
            Logger.getLogger(data_ruta.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    public void crear_archivo(String nombre_archivo, Boolean agregar) throws IOException {
        File archivo = new File(nombre_archivo);

        if (archivo.exists() && !archivo.isFile()) { // Pregunta si el archivo existe, de ser así valida que realmente sea un archivo
            throw new IOException(archivo.getName() + " no es un archivo"); // No no cumple, el programa imprime este mensaje
        }

        escribir = new BufferedWriter(new FileWriter(archivo, agregar));
    }

    public void cerrar_archivo() throws IOException {
        escribir.close();
    }

    public Boolean existe(Ruta object) {
        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;

            while ((line = leer.readLine()) != null) {
                String[] data = line.split("-");

                if (data[0].equals(String.valueOf(object.getIdTer()))) {
                    return true;
                }
            }
            leer.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(data_ruta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(data_ruta.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public Boolean agregar(Ruta object) {
        try {
            this.crear_archivo(this.path, true);

            if (!this.existe(object)) {
                object.setCodigo(this.autoincremental());
                 escribir.write(String.valueOf(object.getCodigo())+"-");
                escribir.write(object.getIdRuta() + "-");
                escribir.write(object.getIdTer() + "-");
                escribir.write(object.getPlaca() + "-");
                escribir.write(String.valueOf(object.getPrecio())+"-");
                escribir.write(object.getFHsalida() + "-");
                escribir.write(object.getOrigen() + "-");
                escribir.write(object.getFHllegada() + "-");
                escribir.write(object.getDestino() + "-");
                escribir.write(object.getDuracion());
                
                
                escribir.write(System.lineSeparator());

                this.cerrar_archivo();
                return true;
            }
        } catch (IOException ex) {
            Logger.getLogger(data_ruta.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return false;
    }

    public Boolean editar(Ruta object) {
        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;
            StringBuilder cadena = new StringBuilder();

            while ((line = leer.readLine()) != null) {
                String[] data = line.split("-");

                if (data[0].equals(String.valueOf(object.getCodigo()))) {
                    cadena.append(String.valueOf(
                            object.getCodigo() + "-"/*
                            + object.getIdTer() + "-"
                            + object.getNombre() + "-"
                            + object.getLugar() + "-"
                            + object.getNumero() + ""*/
                                   
                            + System.lineSeparator())
                    );
                } else {
                    cadena.append(line + System.lineSeparator());
                }
            }
            leer.close();
            this.crear_archivo(this.path, false);
            this.crear_archivo(this.path, true);
            escribir.write(cadena.toString());
            this.cerrar_archivo();
            return true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(data_ruta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(data_ruta.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public Boolean eliminar(Ruta object) {
        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;
            StringBuilder cadena = new StringBuilder();

            while ((line = leer.readLine()) != null) {
                String[] data = line.split("-");

                if (!data[0].equals(String.valueOf(object.getCodigo()))) {
                    cadena.append(line + System.lineSeparator());
                }
            }
            leer.close();
            this.crear_archivo(this.path, false);
            this.crear_archivo(this.path, true);
            escribir.write(cadena.toString());
            this.cerrar_archivo();
            return true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(data_ruta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(data_ruta.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public ArrayList obtener() {
        ArrayList<Ruta> admin = new ArrayList();

        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;
            StringBuilder cadena = new StringBuilder();

            while ((line = leer.readLine()) != null) {
                String[] data = line.split("-");
                Ruta pasa = new Ruta();
                pasa.setCodigo(Integer.parseInt(data[0]));
                pasa.setIdRuta((data[1]));
                pasa.setIdTer(data[2]);
                pasa.setPlaca(data[3]);
                pasa.setPrecio(Integer.parseInt(data[4]));
                
                pasa.setFHsalida(data[5]);
                pasa.setOrigen(data[6]);
                pasa.setFHllegada(data[7]);
                pasa.setDestino(data[8]);
                pasa.setDuracion(data[9]);
                
                admin.add(pasa);
            }
            leer.close();
            return admin;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(data_ruta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(data_ruta.class.getName()).log(Level.SEVERE, null, ex);
        }
        return admin;
    }

    public int autoincremental() {
        int autoincremental = 0;

        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;
            StringBuilder cadena = new StringBuilder();

            while ((line = leer.readLine()) != null) {
                String[] data = line.split("-");

                if (autoincremental < Integer.parseInt(data[0])) {
                    autoincremental = Integer.parseInt(data[0]);
                }
            }
            leer.close();
            return autoincremental + 1;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(data_ruta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(data_ruta.class.getName()).log(Level.SEVERE, null, ex);
        }
        return autoincremental;
    }

}
