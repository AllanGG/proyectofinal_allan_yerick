/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.util.ArrayList;


/**
 *
 * @author admin
 */
public class Logica_Ruta {
    private boolean hay_error;
    private String mensaje_error;
    data_ruta oRut = new data_ruta();
  
    public Boolean agregar(Ruta rut) {
       this.setHay_error(oRut.agregar(rut));
        
        if (!this.isHay_error()) {
            this.setMensaje_error("No se agrego la ruta");
        }
        return this.isHay_error(); 
    }
    
    public Boolean editar(Ruta rut) {
        this.setHay_error(oRut.editar(rut));
        
        if (!this.isHay_error()) {
            this.setMensaje_error("No se edito la ruta");
        }
        return this.isHay_error(); 
    }

    public Boolean eliminar(Ruta rut) {
        this.setHay_error(oRut.eliminar(rut));
        
        if (!this.isHay_error()) {
            this.setMensaje_error("No se elimino la ruta");
        }
        return this.isHay_error(); 
    }
    
    public ArrayList<Ruta> obtener() {
        ArrayList<Ruta> rutas= oRut.obtener();
        return rutas;
    }

    public boolean isHay_error() {
        return hay_error;
    }

    public void setHay_error(boolean hay_error) {
        this.hay_error = hay_error;
    }

    public String getMensaje_error() {
        return mensaje_error;
    }

    public void setMensaje_error(String mensaje_error) {
        this.mensaje_error = mensaje_error;
    }
    
}


