/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author admin
 */
public class Ruta {

    private int codigo;
    private String IdRuta;
    private String idTer;
    private String placa;
    private int precio;
    private String FHsalida;
    private String origen;
    private String FHllegada;
    private String destino;
    private String duracion;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getIdRuta() {
        return IdRuta;
    }

    public void setIdRuta(String IdRuta) {
        this.IdRuta = IdRuta;
    }

    public String getIdTer() {
        return idTer;
    }

    public void setIdTer(String idTer) {
        this.idTer = idTer;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getFHsalida() {
        return FHsalida;
    }

    public void setFHsalida(String FHsalida) {
        this.FHsalida = FHsalida;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getFHllegada() {
        return FHllegada;
    }

    public void setFHllegada(String FHllegada) {
        this.FHllegada = FHllegada;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

}
