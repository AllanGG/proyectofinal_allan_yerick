/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.util.ArrayList;

public class Logica_Unidad {
    private boolean hay_error;
    private String mensaje_error;
    data_unidad oUni = new data_unidad();
  
    public Boolean agregar(Unidad uni) {
        this.setHay_error(oUni.agregar(uni));
        
        if (!this.isHay_error()) {
            this.setMensaje_error("No se agrego el administrador");
        }
        return this.isHay_error(); 
    }
    
    public Boolean editar(Unidad uni) {
        this.setHay_error(oUni.editar(uni));
        
        if (!this.isHay_error()) {
            this.setMensaje_error("No se edito el administrador");
        }
        return this.isHay_error(); 
    }

    public Boolean eliminar(Unidad uni) {
        this.setHay_error(oUni.eliminar(uni));
        
        if (!this.isHay_error()) {
            this.setMensaje_error("No se elimino el administrador");
        }
        return this.isHay_error(); 
    }
    
    public ArrayList<Unidad> obtener() {
        ArrayList<Unidad> unid= oUni.obtener();
        return unid;
    }

    public boolean isHay_error() {
        return hay_error;
    }

    public void setHay_error(boolean hay_error) {
        this.hay_error = hay_error;
    }

    public String getMensaje_error() {
        return mensaje_error;
    }

    public void setMensaje_error(String mensaje_error) {
        this.mensaje_error = mensaje_error;
    }
    
}

