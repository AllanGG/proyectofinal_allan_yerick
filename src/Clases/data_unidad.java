/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.util.ArrayList;


import java.awt.List;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author admin
 */
public class data_unidad {

    private static String path;
    private static BufferedWriter escribir;
    private static BufferedReader leer;

    public data_unidad() {
        try {
            path = new File("src/archivos/Unidades.txt").getCanonicalPath();
        } catch (IOException ex) {
            Logger.getLogger(data_unidad.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    public void crear_archivo(String nombre_archivo, Boolean agregar) throws IOException {
        File archivo = new File(nombre_archivo);

        if (archivo.exists() && !archivo.isFile()) { // Pregunta si el archivo existe, de ser así valida que realmente sea un archivo
            throw new IOException(archivo.getName() + " no es un archivo"); // No no cumple, el programa imprime este mensaje
        }

        escribir = new BufferedWriter(new FileWriter(archivo, agregar));
    }

    public void cerrar_archivo() throws IOException {
        escribir.close();
    }

    public Boolean existe(Unidad object) {
        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;

            while ((line = leer.readLine()) != null) {
                String[] data = line.split("-");

                if (data[0].equals(object.getPlaca())) {
                    return true;
                }
            }
            leer.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(data_unidad.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(data_unidad.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public Boolean agregar(Unidad object) {
        try {
            this.crear_archivo(this.path, true);

            if (!this.existe(object)) {
                object.setCodigo(this.autoincremental());
                 escribir.write(String.valueOf(object.getCodigo())+"-");
                escribir.write(object.getPlaca() + "-");
                escribir.write(String.valueOf(object.getCapacidad()) + "-");
                escribir.write(object.getTerminal() + "");
                
                
                escribir.write(System.lineSeparator());

                this.cerrar_archivo();
                return true;
            }
        } catch (IOException ex) {
            Logger.getLogger(data_unidad.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return false;
    }

    public Boolean editar(Unidad object) {
        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;
            StringBuilder cadena = new StringBuilder();

            while ((line = leer.readLine()) != null) {
                String[] data = line.split("-");

                if (data[0].equals(String.valueOf(object.getCodigo()))) {
                    cadena.append(String.valueOf(
                            object.getCodigo() + "-"
                            + object.getPlaca() + "-"
                            + object.getCapacidad() + "-"
                            + object.getCapacidad() + "-"
                            
                                   
                            + System.lineSeparator())
                    );
                } else {
                    cadena.append(line + System.lineSeparator());
                }
            }
            leer.close();
            this.crear_archivo(this.path, false);
            this.crear_archivo(this.path, true);
            escribir.write(cadena.toString());
            this.cerrar_archivo();
            return true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(data_unidad.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(data_unidad.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public Boolean eliminar(Unidad object) {
        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;
            StringBuilder cadena = new StringBuilder();

            while ((line = leer.readLine()) != null) {
                String[] data = line.split("-");

                if (!data[0].equals(String.valueOf(object.getCodigo()))) {
                    cadena.append(line + System.lineSeparator());
                }
            }
            leer.close();
            this.crear_archivo(this.path, false);
            this.crear_archivo(this.path, true);
            escribir.write(cadena.toString());
            this.cerrar_archivo();
            return true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(data_unidad.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(data_unidad.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public ArrayList obtener() {
        ArrayList<Unidad> admin = new ArrayList();

        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;
            StringBuilder cadena = new StringBuilder();

            while ((line = leer.readLine()) != null) {
                String[] data = line.split("-");
                Unidad pasa = new Unidad();
                pasa.setCodigo(Integer.parseInt(data[0]));
                pasa.setPlaca(data[1]);
                pasa.setCapacidad(Integer.parseInt(data[2]));
                pasa.setTerminal(data[3]);
                
               
                admin.add(pasa);
            }
            leer.close();
            return admin;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(data_unidad.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(data_unidad.class.getName()).log(Level.SEVERE, null, ex);
        }
        return admin;
    }

    public int autoincremental() {
        int autoincremental = 0;

        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;
            StringBuilder cadena = new StringBuilder();

            while ((line = leer.readLine()) != null) {
                String[] data = line.split("-");

                if (autoincremental < Integer.parseInt(data[0])) {
                    autoincremental = Integer.parseInt(data[0]);
                }
            }
            leer.close();
            return autoincremental + 1;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(data_unidad.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(data_unidad.class.getName()).log(Level.SEVERE, null, ex);
        }
        return autoincremental;
    }

}
