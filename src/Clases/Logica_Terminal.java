/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;


import java.util.ArrayList;


/**
 *
 * @author admin
 */
public class Logica_Terminal {
    private boolean hay_error;
    private String mensaje_error;
    data_termi oTer = new data_termi();
  
    public Boolean agregar(Terminal termi) {
        this.setHay_error(oTer.agregar(termi));
        
        if (!this.isHay_error()) {
            this.setMensaje_error("No se agrego la terminal");
        }
        return this.isHay_error(); 
    }
    
    public Boolean editar(Terminal termi) {
        this.setHay_error(oTer.editar(termi));
        
        if (!this.isHay_error()) {
            this.setMensaje_error("No se edito la terminal");
        }
        return this.isHay_error(); 
    }

    public Boolean eliminar(Terminal termi) {
        this.setHay_error(oTer.eliminar(termi));
        
        if (!this.isHay_error()) {
            this.setMensaje_error("No se elimino el administrador");
        }
        return this.isHay_error(); 
    }
    
    public ArrayList<Terminal> obtener() {
        ArrayList<Terminal> termin= oTer.obtener();
        return termin;
    }

    public boolean isHay_error() {
        return hay_error;
    }

    public void setHay_error(boolean hay_error) {
        this.hay_error = hay_error;
    }

    public String getMensaje_error() {
        return mensaje_error;
    }

    public void setMensaje_error(String mensaje_error) {
        this.mensaje_error = mensaje_error;
    }
    
}

