/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import com.toedter.calendar.JDateChooser;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;


public class MetGener {
    public static String aMySQL(String fecha){
        StringTokenizer st= new StringTokenizer(fecha, "/");
        String dia = st.nextToken();
        String mes = st.nextToken();
        String año = st.nextToken();
        String fecha1 = año + "-" + mes + "-" + dia;
        return fecha1;
    }
    public static String leerFecha(JDateChooser j){
        Date date= j.getDate();
        SimpleDateFormat adf = new SimpleDateFormat("dd/MM/yyyy");
        return adf.format(date);
    }
    public static String fechaHoy() {
        String fecha = "";
        Date fechaActual = new Date();
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        fecha = formato.format(fechaActual);
        return fecha;
    }
}
