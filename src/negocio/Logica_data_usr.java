/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import datos.data_usr;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class Logica_data_usr {
    private boolean hay_error;
    private String mensaje_error;
    data_usr oAdm = new data_usr();
  
    public Boolean agregar(Administrador admin) {
        this.setHay_error(oAdm.agregar(admin));
        
        if (!this.isHay_error()) {
            this.setMensaje_error("No se agrego el administrador");
        }
        return this.isHay_error(); 
    }
    
    public Boolean editar(Administrador admin) {
        this.setHay_error(oAdm.editar(admin));
        
        if (!this.isHay_error()) {
            this.setMensaje_error("No se edito el administrador");
        }
        return this.isHay_error(); 
    }

    public Boolean eliminar(Administrador admin) {
        this.setHay_error(oAdm.eliminar(admin));
        
        if (!this.isHay_error()) {
            this.setMensaje_error("No se elimino el administrador");
        }
        return this.isHay_error(); 
    }
    
    public ArrayList<Administrador> obtener() {
        ArrayList<Administrador> admins = oAdm.obtener();
        return admins;
    }

    public boolean isHay_error() {
        return hay_error;
    }

    public void setHay_error(boolean hay_error) {
        this.hay_error = hay_error;
    }

    public String getMensaje_error() {
        return mensaje_error;
    }

    public void setMensaje_error(String mensaje_error) {
        this.mensaje_error = mensaje_error;
    }
    
}

